<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class NewsCollection extends ResourceCollection {

    public static $wrap = false;

    public function toArray($request) {
        return NewsList::collection($this->collection);
    }

}
